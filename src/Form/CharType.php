<?php

namespace App\Form;

use App\Entity\Characters;
use App\Entity\Games;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CharType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', ChoiceType::class,
            [
                'choices'=>[
                    'Dofus'=>'Dofus',
                    'LOL'=>'LOL',
                    'Valorant'=>'Valorant',
                    'Minecraft'=>'Minecraft'
                ]
            ])
            ->add('fk_game', EntityType::class, [
                'class'=>Games::class,
                'choice_label'=>'name',
                'label'=>'Nom de personnage'

            ])
            ->add('submit', SubmitType::class)
        ;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event){
            dd('SIU');
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Characters::class,
        ]);
    }
}
