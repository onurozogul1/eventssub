<?php

namespace App\Controller;

use App\Entity\Characters;
use App\Entity\Games;
use App\Form\CharType;
use App\Form\GameType;
use App\Repository\GamesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{


    #[Route('/home', name: 'app_home')]
    public function index(Request $request, GamesRepository $gamesRepository): Response
    {

        $char = new Characters();
        $form = $this->createForm(CharType::class, $char);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $gamesRepository->add($char, true);


            // actually executes the queries (i.e. the INSERT query)




        }

        return $this->render('home/index.html.twig', [
            'form'=>$form->createView(),
        ]);
    }
}
